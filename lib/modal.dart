import 'package:flutter/material.dart';
import 'Login.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'check_box.dart';
class Modal extends StatefulWidget {
  @override
  Modal_state createState() {
    return new Modal_state();
  }
}
class Modal_state extends State<Modal> {
    bool _isButtonDisabled = true;

  @override
  Widget build(BuildContext context) {
        Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Stack(

        children: <Widget>[
          Center(
            
      child: new Image.asset(
              'assets/bg.jpg',
              width: size.width,
              height: size.height,
              fit: BoxFit.fill,
            ),
    ),
    Container(
      padding: EdgeInsets.only(top: 100),
      child: Column(
        
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(top: 40),
      child: FlatButton(
        child: Text('VEUILLEZ CHOISIR LES THEMES QUI VOUS INTERESSENT ', style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 28.0,
                    color: Colors.white,
                    ),
                    textAlign: TextAlign.center,),
        onPressed: () {  setState(() => _isButtonDisabled = !_isButtonDisabled);
          showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Center(
                  child: ChekBox(),
                  
                );
                
              });
        },
        
      ),
      
    ),
    
Container(
                  padding: EdgeInsets.only(top: 60),

        child :
        new MaterialButton(
                          height: 50.0,
                          minWidth: 150.0,
                          color: Colors.green,
                          splashColor: Colors.teal,
                          textColor: Colors.white,
                          child: new Icon(FontAwesomeIcons.signInAlt),
                          onPressed:() {
                            
Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => LoginPage(),),);  },
  ),
      )
      ],

    ), 
    ),
    
    
    
    ],
    ),
    );
  }

  
}