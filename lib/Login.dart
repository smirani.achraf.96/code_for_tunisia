import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:validate/validate.dart';
import 'citoyen/Home.dart';
import 'sign.dart';
import 'Reset.dart';
import 'dart:convert';
import 'events.dart';
import 'package:http/http.dart' as http;


LoginPageState loginState;
class LoginPage extends StatefulWidget {
  @override
  State createState() {
    loginState=LoginPageState();
    return loginState;}
}
class _LoginData {
 
}
class LoginPageState extends State<LoginPage>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
   final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  _LoginData _data = new _LoginData();
   String email = '';
  String password = '';
  String emailUser;
  String nameUser;
  String typeUser;
  String _validateEmail(String value) {
    // If empty value, the isEmail function throw a error.
    // So I changed this function with try and catch.
    try {
      Validate.isEmail(value);
    } catch (e) {
      return '';
    }

    return null;
  }

  String _validatemot_de_passe(String value) {
    if (value.length < 4) {
      return '';
    }

    return null;
  }

  void submit() {
    // First validate form.
    if (this._formKey.currentState.validate()) {
      _formKey.currentState.save(); // Save our form now.

      print('Printing the login data.');
      
    }
  }
 signIn(String email,String password) async {
  String url = 'http://192.168.1.2:3000/users/auth/citoyen';
  Map<String, String> headers = {"Content-type": "application/json"};
  Map<dynamic,String> jsons = {"email": email, "mot_de_passe": password};
  var body=json.encode(jsons);
      print("***************************AUTH******************");
print(body);

  try {
    print("***************************AUTH******************");
    return http.post(url,body:body,headers:headers ).then((http.Response response) {

  
    Map<String,dynamic> body=json.decode(response.body);

    String status =(body['status']);
    print(status);
    String message =(body['message']);
    print(message);
    String token =(body['data']['token']);
    print(token);

    
    

    print(token);

    if(status=="success"){
          print("***************************SUCCES******************");

        setState(() {
        
         email=email;
         
        });
       Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => HomePage(),),);

    }else(){
      showDialog(
        context: context,
        builder: (_) => new AlertDialog(
            title: new Text("Atention"),
            content: new Text("Email ou Mot de passe invalide"),));
    };
           

    
  });
  } catch (e) { 
    print("error");
    print(e);
  }
}



  @override
  void initState() {
    super.initState();
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: new Stack(fit: StackFit.expand, children: <Widget>[
        new Image(
          image: new AssetImage("assets/logo1.png"),
          fit: BoxFit.cover,
          colorBlendMode: BlendMode.darken,
          color: Colors.black87,
        ),
        new Theme(
          data: new ThemeData(
              brightness: Brightness.dark,
              inputDecorationTheme: new InputDecorationTheme(
                // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
                labelStyle:
                    new TextStyle(color: Colors.tealAccent, fontSize: 25.0),
              )),
          isMaterialAppTheme: true,
          child: new ListView(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              new Container(
                padding: const EdgeInsets.all(40.0),
                child: new Form(
                                              key: _formKey,


                  autovalidate: true,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      
                      new TextFormField(
                        decoration: new InputDecoration(

                            labelText: "Email",
                            hintText: 'you@example.com',
                             fillColor: Colors.white),
                        keyboardType: TextInputType.emailAddress,
                         validator: this._validateEmail,
                      onSaved: (String value) {
                  setState(() {
                    print("5555555555555");
                   email = value; 
                  });
                }
                      ),
                      new TextFormField(
                        
                        decoration: new InputDecoration(
                          labelText: "Mot de Passe",
                          hintText: 'Mot de passe',
                          
                        ),
                        obscureText: true,
                        keyboardType: TextInputType.text,
                         validator: this._validatemot_de_passe,
                onSaved: (String value) {
                  print("666666666666666666");
setState(() {
               password = value;
 
});                }
                      ),
                      new Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                      ),
                      new MaterialButton(
                        
                        height: 50.0,
                        minWidth: 150.0,
                        color: Colors.green,
                        splashColor: Colors.teal,
                        textColor: Colors.white,
                        
                        child: new Icon(FontAwesomeIcons.signInAlt),
                        onPressed: () {
                          print("****************EMAIL");
                          print(email);
                                                    print("****************PASSWORD");
                                                    print(password);
                          if (_formKey.currentState.validate()) {
                                        _formKey.currentState.save();
signIn(email,password);                                        } else {
                                        print("eroor");
                                      }

                          

                          
                
  },
                        
                        
                      ),
                      
                      Padding(
            padding: EdgeInsets.only(top: 10.0),
            
            child: FlatButton(
             
                onPressed: () { 
                  
               
Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => Events(),),);
                },
                child: Text(
                  "Mot de Passe Oublié?",
                  style: TextStyle(
                      decoration: TextDecoration.underline,
                      color: Colors.white,
                      fontSize: 16.0,
                      fontFamily: "WorkSansMedium"),
              )),
          
          ),
          

           
            Text("\n Vous n'etes pas encore membre de l'equipe ?",style: (TextStyle(color: Colors.red.withOpacity(0.6),fontSize: 15.0,)),
           textAlign: TextAlign.center,
            ),
            
            
            FlatButton(
              
            child: Text("Inscrivez vous !",style: (TextStyle(color: Colors.white.withOpacity(0.6),fontSize: 22.0,)),
             textAlign: TextAlign.center,
                
    
              ),
              
            
            onPressed: (){
               
Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => SignUpPage(),),);
  

            },
          ),


          
                    ],
                  ),
                ),
              )
            ], 
          ),
        ),
      ]),
      
    );
  }
  
}