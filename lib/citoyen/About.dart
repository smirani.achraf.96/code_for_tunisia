import 'package:code_for_tunisia/citoyen/forum.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;
import 'events.dart';
import 'projets.dart';
import 'formation.dart';
import 'Home.dart';
import '../Login.dart';

class About extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AboutState();
  }
}
class _AboutState extends State<About> {
  
  Widget _buildContent() {
    
    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildAvatar(),
          _buildInfo(),
          
        ],
      ),
    );
  }

    Widget _buildAvatar() {
    return Container(
      width: 110.0,
      height: 110.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white30),
      ),
      margin: const EdgeInsets.only(top: 32.0, left: 16.0),
      padding: const EdgeInsets.all(3.0),
      child: ClipOval(
        child: Image.asset("assets/logo1.png"),
      ),
    );
  }

  Widget _buildInfo() {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
           ' CODE FOR TUNISIA ',
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 30.0,
            ),
          ),
          Text(
            'Sousse,Tunisie',
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(
            color: Colors.white.withOpacity(0.85),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: 225.0,
            height: 1.0,
          ),
          Text(
            "Code For Tunisia est une organisation non gouvernementale tunisienne qui a pour but d'aider la societé à apprendre le developpement & à presenter des projets qui  aident le gouvernement à travailler pour les personnes qui en ont le plus besoin.",
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              height: 1.4,
            ),
          ),
        ],
      ),
    );
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
 appBar: new AppBar(title: new Text("A propos"), backgroundColor: Colors.redAccent, actions: <Widget>[

       FloatingActionButton(
         heroTag: Null,
              child: Icon(Icons.highlight_off),
              backgroundColor: Colors.redAccent,
              onPressed: (){
                 Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
              }
            ),
            ],
            ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: new Text("Cher Citoyen"),
              accountName: new Text("Bienvenue"),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new AssetImage("assets/logo1.png"),
                ),
                onTap: () => print("This is your current account."),
              ),
              
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/logo1.png"),
                  fit: BoxFit.fill
                )
              ),
            ),
            new ListTile(
              title: new Text("Acceuil"),
              trailing: new Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pop();
               Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) =>HomePage()));
              }
            ),
            new ListTile(
              title: new Text("A propos de nous"),
              
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => null));
              }
            ),
            new ListTile(
              title: new Text("Les evenements"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
              }
            ),
            new ListTile(
              title: new Text("Les formations"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Formation()));
              }
            ),
            new ListTile(
              title: new Text("Les projets"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Projets()));
              }
            ),
            new ListTile(
              title: new Text("Blog"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Forum()));
              }
            ),
            new ListTile(
              title: new Text("Contacte"),
              
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => null));
              }
            ),
            
          ],
        ),
      ),
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset("assets/logo1.png", fit: BoxFit.cover),
          BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              color: Colors.black.withOpacity(0.5),
              child: _buildContent(),
            ),
          ),
        ],
      ),
    )
    ;
  }
}
  
  