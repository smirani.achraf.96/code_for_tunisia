import 'dart:async';
import 'dart:convert';
import 'package:code_for_tunisia/citoyen/About.dart';
import 'package:code_for_tunisia/citoyen/Home.dart';

import '../Login.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../post.dart';
import '../add.dart';
import 'dart:ui' as ui;



class Forum extends StatefulWidget {
  @override
  _ForumState createState() => new _ForumState();
}

class _ForumState extends State<Forum> {

  List data;

  Future<String> getData() async {
    var response = await http.get(
      Uri.encodeFull("http://192.168.1.2:3000/blog/all"),
      headers: {
        "Accept": "application/json"
      }
    );

    this.setState(() {
      data = jsonDecode(response.body);
    });
    
    //print(data[1]["title"]);
    
    return "Success!";
  }

  @override
  void initState(){
    this.getData();
  }

  @override
  Widget build(BuildContext context){
    return new Scaffold(
      appBar: new AppBar(title: new Text("Blog"), backgroundColor: Colors.redAccent, actions: <Widget>[

       FloatingActionButton(
         heroTag: 'z',
              child: Icon(Icons.lock_outline),
              backgroundColor: Colors.redAccent,
              onPressed: (){
                 Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
              }
            ),
            ],
            ),
            drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: new Text("Cher Citoyen"),
              accountName: new Text("Bienvenue"),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new AssetImage("assets/logo1.png"),
                ),
                onTap: () => print("This is your current account."),
              ),
              
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/logo1.png"),
                  fit: BoxFit.fill
                )
              ),
            ),
            new ListTile(
              title: new Text("Acceuil"),
              trailing: new Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pop();
               Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) =>HomePage()));
              }
            ),
            new ListTile(
              title: new Text("A propos de nous"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => About()));
              }
            ),
            new ListTile(
              title: new Text("Les evenements"),
              
              onTap: () {
                Navigator.of(context).pop();
                // Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
              }
            ),
            new ListTile(
              title: new Text("Les formations"),
              
              onTap: () {
                Navigator.of(context).pop();
                // Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Formation()));
              }
            ),
            new ListTile(
              title: new Text("Les projets"),
              
              onTap: () {
                Navigator.of(context).pop();
                // Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Projets()));
              }
            ),
            new ListTile(
              title: new Text("Blog"),
              
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => null));
              }
            ),
            new ListTile(
              title: new Text("Contacte"),
              
              onTap: () {
                Navigator.of(context).pop();
                // Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Contact()));
              }
            ),
            
          ],
        ),
      ), 
      floatingActionButton: new FloatingActionButton(
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=> AddPost()));
          },
          backgroundColor: Colors.green,
          //if you set mini to true then it will make your floating button small
          mini: true,
          child: new Icon(Icons.add),
      ),
     body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset("assets/logo1.png", fit: BoxFit.cover),
          BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              color: Colors.black.withOpacity(0.5),
              child: ListView.builder(
        itemCount: data == null ? 0 : data.length,
        itemBuilder: (BuildContext context, int index){
          return new Card(
            child: FlatButton(
              child: Text(data[index]["title"],textAlign: TextAlign.left,), 
              onPressed: (){
                Navigator.push(context, MaterialPageRoute(builder: (context)=> Post(data[index]['title'],data[index]['body'])));
              },
            ),
             
          );
        },
      ),
    )
    )
    ]
    )
    );
  }
}
