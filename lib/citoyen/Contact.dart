import 'package:code_for_tunisia/citoyen/forum.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'Home.dart';
import 'events.dart';
import 'projets.dart';
import 'formation.dart';
import '../Login.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
class Post {
  
String msg;  String titre;
   
  Post({this.titre,this.msg});
 
  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      titre: json['titre'],
      msg: json['msg'],

    );
  }
 
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["titre"] = titre;
     map["msg"] = msg;

    return map;
  }
}
Future<Post> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Post.fromJson(json.decode(response.body));
  });
}
class Contact extends StatefulWidget {
  @override
  State createState() => new ContactState();
}

class ContactState extends State<Contact>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
   final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
    static final CREATE_POST_URL = 'http://192.168.1.2:3000/contact/add';
  TextEditingController titreControler = new TextEditingController();
  TextEditingController msgControler = new TextEditingController();
  

  @override
  void initState() {
    super.initState();
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
 appBar: new AppBar(title: new Text("Contacte"), backgroundColor: Colors.redAccent, actions: <Widget>[

       FloatingActionButton(
         heroTag: Null,
              child: Icon(Icons.highlight_off),
              backgroundColor: Colors.redAccent,
              onPressed: (){
                 Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
              }
            ),
            ],
            ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: new Text("Cher Citoyen"),
              accountName: new Text("Bienvenue"),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new AssetImage("assets/logo1.png"),
                ),
                onTap: () => print("This is your current account."),
              ),
              
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/logo1.png"),
                  fit: BoxFit.fill
                )
              ),
            ),
            new ListTile(
              title: new Text("Acceuil"),
              trailing: new Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pop();
               Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) =>HomePage()));
              }
            ),
            new ListTile(
              title: new Text("A propos de nous"),
              
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => null));
              }
            ),
            new ListTile(
              title: new Text("Les evenements"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
              }
            ),
            new ListTile(
              title: new Text("Les formations"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Formation()));
              }
            ),
            new ListTile(
              title: new Text("Les projets"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Projets()));
              }
            ),
            new ListTile(
              title: new Text("Blog"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Forum()));
              }
            ),
            new ListTile(
              title: new Text("Contacte"),
              
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => null));
              }
            ),
            
          ],
        ),
      ),
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: new Stack(fit: StackFit.expand, children: <Widget>[
        new Image(
          image: new AssetImage("assets/logo1.png"),
          fit: BoxFit.cover,
          colorBlendMode: BlendMode.darken,
          color: Colors.black87,
        ),
        new Theme(
          data: new ThemeData(
              brightness: Brightness.dark,
              inputDecorationTheme: new InputDecorationTheme(
                // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
                labelStyle:
                    new TextStyle(color: Colors.tealAccent, fontSize: 25.0),
              )),
          isMaterialAppTheme: true,
          child: new ListView(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              new Container(
                padding: const EdgeInsets.all(40.0),
                child: new Form(
                   key: _formKey,
                  autovalidate: true,

                      child: new SingleChildScrollView(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new TextFormField(
                        controller: titreControler,
                        decoration: new InputDecoration(

                            labelText: "Titre",
                             fillColor: Colors.white),
                        keyboardType: TextInputType.text,
               
                      ),new TextFormField(
                        controller: msgControler,
                        decoration: new InputDecoration(

                            labelText: "Message",
                             fillColor: Colors.white),
                        keyboardType: TextInputType.text,
                        scrollPadding: EdgeInsets.all(50),
                        maxLines: 20,
                      ),
                    
                 
                      new Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                      ),
                      new MaterialButton(
                        height: 50.0,
                        minWidth: 150.0,
                        color: Colors.green,
                        splashColor: Colors.teal,
                        textColor: Colors.white,
                        child: new Icon(FontAwesomeIcons.signInAlt),
                        onPressed: () async{ 
                          Post newPost = new Post(
                        titre: titreControler.text,
                        msg: msgControler.text,
                        );
                    Post p = await createPost(CREATE_POST_URL,
                        body: newPost.toMap());
                    print(p.titre);
                    print(p.msg);
                    
                                 if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                 }
showDialog(
        context: context,
        builder: (_) => new AlertDialog(
            title: new Text("Merci"),
            content: new Text("Votre message a été envoyé"),
            actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Fermer"),
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Contact()));
              },
            ),
          ],
        ),
    );
  },
  ),
   ],
                  ),
                ),
              ),
               ),
                ], 
          ),
        ),
      ]),
      
    );
    
  }
  
}