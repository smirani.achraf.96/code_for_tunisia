import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:ui' as ui;
import 'projets.dart';
import 'package:code_for_tunisia/models/Mformation.dart';
import 'Home.dart';
import 'Contact.dart';
import 'About.dart';
import '../Login.dart';
import 'dart:convert';
import 'package:code_for_tunisia/API.dart';
import 'package:http/http.dart' as http;
import 'events.dart';
import 'forum.dart';
class Post {
  
String description;  String titre;
  String lieu; 
  Post({this.description,this.titre,this.lieu});
 
  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      description: json['description'],
      titre: json['titre'],
      lieu: json['lieu'],

    );
  }
 
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["titre"] = titre;
     map["description"] = description;
    map["lieu"] = lieu;

    return map;
  }
}
Future<Post> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Post.fromJson(json.decode(response.body));
  });
}
class Formation extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FormationState();
  }
}
class _FormationState extends State<Formation> {
var users = new List<Mformation>();
final Future<Post> post;
 
  _FormationState({Key key, this.post}); 
    final _formKey = GlobalKey<FormState>();

  static final CREATE_POST_URL = 'http://192.168.1.2:3000/categorie/add';
  TextEditingController titreControler = new TextEditingController();
  TextEditingController descriptionControler = new TextEditingController();
    TextEditingController lieuControler = new TextEditingController();

 

  _getFormation() {
    API.getFormation().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        users = list.map((model) => Mformation.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getFormation();
  }

  dispose() {
    super.dispose();
  }

      bool _isButtonDisabled = true;
     
     
  
  Widget _buildContent(Mformation user) {
    
    return SingleChildScrollView(
      
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          
         
           
          _buildInfo1(user),
           
          
        ],
      ),
    );
  }
  
Widget submit() {
  return Container(
                padding: EdgeInsets.only(left: 250),

      child :
      new MaterialButton(
                        height: 40.0,
                        minWidth: 80.0,
                        color: Colors.green,
                        splashColor: Colors.teal,
                        textColor: Colors.white,
                        child:  Icon(_isButtonDisabled ? FontAwesomeIcons.signInAlt : FontAwesomeIcons.windowClose),
                        onPressed:_isButtonDisabled ? null : () {setState(() => _isButtonDisabled = !_isButtonDisabled);  },
  ),
    );
    }

  

  Widget _buildInfo1(Mformation user) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
           user.titre,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 30.0,
            ),
          ),
          Text(
           user.lieu,
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(
            color: Colors.white.withOpacity(0.85),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: 225.0,
            height: 1.0,
          ),
          Text(user.description,
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              height: 1.4,
            ),
          ),
        ],
      ),
    );
  }


  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
                        resizeToAvoidBottomPadding: false ,

 appBar: new AppBar(title: new Text("Formations"), backgroundColor: Colors.redAccent, actions: <Widget>[

       FloatingActionButton(
         heroTag: 'z',
              child: Icon(Icons.lock_outline),
              backgroundColor: Colors.redAccent,
              onPressed: (){
                 Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginPage()));
              }
            ),
            ],
            ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: new Text("Cher Citoyen"),
              accountName: new Text("Bienvenue"),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new AssetImage("assets/logo1.png"),
                ),
                onTap: () => print("This is your current account."),
              ),
              
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/logo1.png"),
                  fit: BoxFit.fill
                )
              ),
            ),
            new ListTile(
              title: new Text("Acceuil"),
              trailing: new Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pop();
               Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) =>HomePage()));
              }
            ),
            new ListTile(
              title: new Text("A propos de nous"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => About()));
              }
            ),
            new ListTile(
              title: new Text("Les evenements"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
              }
            ),
            new ListTile(
              title: new Text("Les formations"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Formation()));
              }
            ),
            new ListTile(
              title: new Text("Les projets"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Projets()));
              }
            ),
            new ListTile(
              title: new Text("Blog"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Forum()));
              }
            ),
            new ListTile(
              title: new Text("Contacte"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Contact()));
              }
            ),
            
          ],
        ),
      ), 
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset("assets/logo1.png", fit: BoxFit.cover),
          BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              color: Colors.black.withOpacity(0.5),
              child: ListView.builder(
                itemCount: users.length,
                itemBuilder: (BuildContext context,int index){
                  return _buildContent(users[index]);
                },
              ),


             
            ),
          ),
        ],
      ),
    )
    ;
  }
}
  
  