import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'inscriptionC.dart';
import 'inscriptionB.dart';
class SignUpPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _SignUpPageState();
  }
}

class _SignUpPageState extends State<SignUpPage>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("assets/logo1.png"),
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.darken,
            color: Colors.black87,
          ),
          new Theme(
            data: new ThemeData(
                brightness: Brightness.dark,
                inputDecorationTheme: new InputDecorationTheme(
                  // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
                  labelStyle:
                      new TextStyle(color: Colors.tealAccent, fontSize: 25.0),
                )),
            isMaterialAppTheme: true,

           
            
           child: 
           
          SingleChildScrollView(
                      child: Column(
              children: <Widget>[
              
    Container(
      padding: EdgeInsets.all(30),
      child: 
      Column(
                    children:<Widget>[
      Text("Comment voulez vous s'inscrire ?",style:new TextStyle(color: Colors.white)),
                     ],
                     ), 
                     ),
                     Container(
      padding: EdgeInsets.all(30),
      child: 
      Column(
                    children:<Widget>[
      Text("✓Simple Citoyen : \n En choisissant cet option vous beneficiez de toutes les services fournies par l'associations qu'elle que soit les formations ou les evenements organisés par les membres .\n Si vous etes debutant dans le domaine de developpement et vous voulez apprendre , il est conseiller de s'incrire comme simple citoyen . \n Pour s'incrire comme simple citoyen cliquer ci-dessous",style:new TextStyle(color: Colors.white)),
      MaterialButton(
                              height: 50.0,
                              minWidth: 100.0,
                              color: Colors.green,
                              splashColor: Colors.teal,
                              textColor: Colors.white,
                                 shape: CircleBorder(),
                              child: new Icon(FontAwesomeIcons.signInAlt),
                              onPressed: () {Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => InscirptionBPage(),),);
}),
                     ],
                     ), 
                     ),
                Container(
      padding: EdgeInsets.all(30),
      child: 
      SingleChildScrollView(
                child: Column(
                      children:<Widget>[
        Text("✓Membre Benevole : \n En choisissant cet option vous devez avoir des competences dans le domaine informatique . Vous serez chargé de participer à l'organisation des evenements et d'assurer les formations selon le planning de l'administration .\n Si vous avez l'esprit de la vie associative et vous presentez les qualités requises veuillez vous s'incrire comme membre benevole . \n Pour s'incrire comme membre benevole cliquer ci-dessous",style:new TextStyle(color: Colors.white)),
        MaterialButton(
                                height: 50.0,
                                minWidth: 100.0,
                                color: Colors.green,
                                splashColor: Colors.teal,
                                textColor: Colors.white,
                                   shape: CircleBorder(),
                                child: new Icon(FontAwesomeIcons.signInAlt),
                                onPressed: (){Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => InscirptionCPage(),),);
}),
                       ],
                       ),
      ), 
                     ),
                
               ],
              
            ),
          ),
          
            
            
            
            
          ),
        ],
      ),
    );
  }
}
