import 'package:flutter/material.dart';

class Reset extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ResetState();
  }
}
class _ResetState extends State<Reset>
{
  @override
 Widget build(BuildContext context) {

   return new Scaffold(
     backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          new Image(
            image: new AssetImage("assets/logo1.png"),
            fit: BoxFit.cover,
            colorBlendMode: BlendMode.darken,
            color: Colors.black87,
          ),],)
                );
 }
 }