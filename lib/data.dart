class IntroItem {
  IntroItem({
    this.title,
    this.category,
    this.imageUrl,
  });

  final String title;
  final String category;
  final String imageUrl;
}

final sampleItems = <IntroItem>[
  new IntroItem(title: "Pour connaître et participer aux futurs rendez-vous IT, ou obtenir des informations sur les événements passés, n'hésitez pas à consulter notre rubrique evenements.", category: 'Evenements', imageUrl: 'assets/t2.png',),
  new IntroItem(title: 'Suivre les cours de formations garantie par des developpeurs et des benevoles experts, pour apprendre et participer aux developpement technologique du pays  ', category: 'Formations', imageUrl: 'assets/e2.png',),
  new IntroItem(title: "Vos compétences informatiques au service d’un projet humanitaire.Vous trouverez dans notre rubrique Projets des dizaines de projets dedié aux relations entre le citoyen et le gouvernement .", category: 'Projets', imageUrl: 'assets/t1.png',),
];