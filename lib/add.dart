import 'dart:convert';
import 'forum.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:http/http.dart' as http;

class Add {
  
String title;  String body;
  Add({this.title,this.body});
 
  factory Add.fromJson(Map<String, dynamic> json) {
    return Add(
      title: json['title'],
      body: json['body'],
    

    );
  }
 
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["title"] = title;
     map["body"] = body;

    return map;
  }
}
Future<Add> createadd(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Add.fromJson(json.decode(response.body));
  });
}

class AddPost extends StatefulWidget {
  @override
  _AddPostState createState() => _AddPostState();
}

class _AddPostState extends State<AddPost> {
  final Future<Add> add;
 
  _AddPostState({Key key, this.add}); 
    final _fromkey = GlobalKey<FormState>();

  static final CREATE_POST_URL = 'http://192.168.1.2:3000/blog/add';
  TextEditingController titleControler = new TextEditingController();
  TextEditingController bodyControler = new TextEditingController();
  String _title, _body;
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add Post"),
      ),
      body:Container(
        padding: EdgeInsets.all(30),
        child: Form(
        key: _fromkey,
        child: ListView(
          children: <Widget>[
            //email field-------------------------------------------------------/

            TextFormField(
                     controller: titleControler,

              validator: (input) {
                if (input.isEmpty) {
                  return "enter a title";
                }
              },
              onSaved: (input) {
                _title = input;
              },
              decoration: InputDecoration(labelText: "Title"),
              keyboardType: TextInputType.text,
              
            ),
            //Body field---------------------------------------------------- */
            TextFormField(
            controller: bodyControler,

              validator: (input) {
                if (input.length<1) {
                  return "enter body text";
                }
              },
              onSaved: (input) {
                _body = input;
              },
              decoration: InputDecoration(labelText: "Body"),
              
            ),
            RaisedButton(onPressed:() async{
                                
                                Add newPost = new Add(
                        title: titleControler.text,
                        body: bodyControler.text,
                       );
                    Add p = await createadd(CREATE_POST_URL,
                        body: newPost.toMap());
                    print(p.title);
                    print(p.body);
                                 if (_fromkey.currentState.validate()) {
                                _fromkey.currentState.save();
                                 }
                             Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Forum()));
                              },
            child: Text("Add !"),
            color: Colors.green,

            ),
          ],
        ),
      ) ,

      ), 
      
    );
  }
}
