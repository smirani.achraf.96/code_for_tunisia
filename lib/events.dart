import 'dart:core';
import 'package:code_for_tunisia/forum.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:ui' as ui;
import 'projets.dart';
import 'formation.dart';
import 'Home.dart';
import 'Contact.dart';
import 'About.dart';
import 'LoginB.dart';
import 'dart:async';
import 'dart:convert';
import 'package:code_for_tunisia/API.dart';
import 'package:code_for_tunisia/models/User.dart';
import 'package:http/http.dart' as http;


String ss="participer";

class Post {
  
String detail;  String titre;
  String lieu; String id;
  Post({this.detail,this.titre,this.lieu,this.id});
 
  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      detail: json['detail'],
      titre: json['titre'],
      lieu: json['lieu'],
      id:json['_id']

    );
  }
 
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["titre"] = titre;
     map["detail"] = detail;
    map["lieu"] = lieu;

    return map;
  }
}
Future<Post> createPost(String url, {Map body}) async {
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Post.fromJson(json.decode(response.body));
  });
}
class Part {
  
int nb;
int id;
  Part({this.nb,this.id});
 
  factory Part.fromJson(Map<String, dynamic> json) {
    return Part(
      nb: json['nbrParticipant'],
      id: json['id']
    );
  }
 
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["nbrParticipant"] = nb;
    
  
    return map;
  }
}
Future<Part> partPost(String url, {Map body}) async {
  print("****************************BODY*****************************");
  return http.post(url, body: json.decode(json.encode(body))).then((http.Response response) {
    final int statusCode = response.statusCode;
    print("****************************RESPONSE*****************************");
    print(response.body); 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Part.fromJson(json.decode(response.body));
  });
}
_EventsState evenState;
class Events extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    evenState= _EventsState();
   return evenState;
  } 
}
class _EventsState extends State<Events> {
  IconData icon = Icons.event_available;

// Widget returnButtonContent(){
//   if (typeUser!="Citoyen") {
//     return
     
//    }
// }


var users = new List<User>();
String typeUser;
final Future<Post> post;
 
  _EventsState({Key key, this.post}); 
    final _formKey = GlobalKey<FormState>();

  static final CREATE_POST_URL = 'http://192.168.1.2:3000/evenement/add';
  static final partUrl = 'http://192.168.1.2:3000/evenement/participate';
  TextEditingController titreControler = new TextEditingController();
  TextEditingController detailControler = new TextEditingController();
    TextEditingController lieuControler = new TextEditingController();

 

  _getUsers() {
    API.getUsers().then((response) {
      setState(() {
        Iterable list = json.decode(response.body);
        users = list.map((model) => User.fromJson(model)).toList();
      });
    });
  }

  initState() {
    super.initState();
    _getUsers();
  }

  dispose() {
    super.dispose();
  }

      bool _isButtonDisabled = true;
     
     
  
  Widget _buildContent(User user) {
    
    return SingleChildScrollView(
      
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          
         
           
          _buildInfo1(user),
           
          
        ],
      ),
    );
  }
  
Widget submit() {
  return Container(
                padding: EdgeInsets.only(left: 250),

      child :
      new MaterialButton(
                        height: 40.0,
                        minWidth: 80.0,
                        color: Colors.green,
                        splashColor: Colors.teal,
                        textColor: Colors.white,
                        child:  Icon(_isButtonDisabled ? FontAwesomeIcons.signInAlt : FontAwesomeIcons.windowClose),
                        onPressed:_isButtonDisabled ? null : () {setState(() => _isButtonDisabled = !_isButtonDisabled);  },
  ),
    );
    }

  
   

  Widget _buildInfo1(User user) {
    return Padding(
      padding: const EdgeInsets.only(top: 16.0, left: 16.0, right: 16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
               
          Container(
      width: 110.0,
      height: 110.0,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        border: Border.all(color: Colors.white30),
      ),
      margin: const EdgeInsets.only(top: 32.0, left: 16.0),
      padding: const EdgeInsets.all(3.0),
      child: ClipOval(
        
        child: Image.network(user.images)
      ),
    ),
        
          Text(
           user.titre,
            style: TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
              fontSize: 30.0,
            ),
          ),
          Text(
           user.lieu,
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              fontWeight: FontWeight.w500,
            ),
          ),
          Container(
            color: Colors.white.withOpacity(0.85),
            margin: const EdgeInsets.symmetric(vertical: 16.0),
            width: 225.0,
            height: 1.0,
          ),
          Text(user.detail,
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              height: 1.4,
            ),
          ),
          // Container(
          //    child:ListTile(
          //   title: Image.network(
          //      user.images)
          //  ) ),
     
           
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
                        resizeToAvoidBottomPadding: false ,

 appBar: new AppBar(title: new Text("Evenements"), backgroundColor: Colors.redAccent, actions: <Widget>[
        FloatingActionButton(
         heroTag: Null,
              mini: true,
          child: new Icon(Icons.add),
              backgroundColor: Colors.redAccent,
              // onPressed:()
              // {Navigator.of(context).pop();
              //   Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => addevent())); },
              onPressed: ()async{
                showDialog(
                context: context,
                builder: (BuildContext context) {

                  return AlertDialog(
                    
                    content: Form(
                      
                      key: _formKey,
                      child: new SingleChildScrollView(
          child:
                      
                      Column(
                        
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          
                          Padding(
                            
                            padding: EdgeInsets.all(8.0),
                            child: TextFormField(
                              controller: detailControler,
                  decoration: InputDecoration(
                      hintText: "body....", labelText: 'Post Body'),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: TextFormField(
                              controller: titreControler,
                  decoration: InputDecoration(
                      hintText: "title....", labelText: 'Post Title'),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(8.0),
                            child: TextFormField(
                              controller: lieuControler,
                  decoration: InputDecoration(
                      hintText: "body....", labelText: 'Post Body'),
                            ),
                          ),
                          
         
        
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: RaisedButton(
                              child: Text("Submit"),
                              onPressed: () async{
                                
                                Post newPost = new Post(
                        titre: titreControler.text,
                        detail: detailControler.text,
                        lieu: lieuControler.text);
                    Post p = await createPost(CREATE_POST_URL,
                        body: newPost.toMap());
                    print(p.titre);
                    print(p.detail);
                    print(p.lieu);
                                 if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                 }
                             Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    )  );
                });
              }
            ),
 
       FloatingActionButton(
         heroTag: 'z',
         mini: true ,
              child: Icon(Icons.lock_outline),
              backgroundColor: Colors.redAccent,
              onPressed: (){
                 Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginBPage()));
              }
            ),
            ],
            ),
            // bottomNavigationBar: (FancyTabBar()),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: new Text("Cher Benevole"),
              accountName: new Text("Bienvenue"),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new AssetImage("assets/logo1.png"),
                ),
                onTap: () => print("This is your current account."),
              ),
              
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/logo1.png"),
                  fit: BoxFit.fill
                )
              ),
            ),
            new ListTile(
              title: new Text("Acceuil"),
              trailing: new Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pop();
               Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) =>HomePage()));
              }
            ),
            new ListTile(
              title: new Text("A propos de nous"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => About()));
              }
            ),
            new ListTile(
              title: new Text("Les evenements"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
              }
            ),
            new ListTile(
              title: new Text("Les formations"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Formation()));
              }
            ),
            new ListTile(
              title: new Text("Les projets"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Projets()));
              }
            ),
            new ListTile(
              title: new Text("Blog"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Forum()));
              }
            ),
            new ListTile(
              title: new Text("Contacte"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Contact()));
              }
            ),
            
          ],
        ),
      ), 
      
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Image.asset("assets/logo1.png", fit: BoxFit.cover),
          
 BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
            child: Container(
              color: Colors.black.withOpacity(0.5),
              child: ListView.builder(
                itemCount: users.length,
                itemBuilder: (BuildContext context,int index){
                  return _buildContent(users[index],);
                },
                
              ),


             
            ),
          ),
            
        
        ],
        
      ),
      
    )
    ;
  }
}
  