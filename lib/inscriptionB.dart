import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:validate/validate.dart';
import 'modal.dart';

import 'package:http/http.dart' as http;
import 'dart:convert';
class Inscri {
  
String nom;  String prenom;
  String email; String mot_de_passe ; String adress ;
  Inscri({this.nom,this.prenom,this.email,this.mot_de_passe,this.adress});
 
  factory Inscri.fromJson(Map<String, dynamic> json) {
    return Inscri(
      nom: json['nom'],
      prenom: json['prenom'],
      email: json['email'],
      mot_de_passe: json['mot_de_passe'],
      adress: json['adress'],
    );
  }
 
  Map toMap() {
    var map = new Map<String, dynamic>();
    map["nom"] = nom;
     map["prenom"] = prenom;
    map["email"] = email;
    map["mot_de_passe"]=mot_de_passe;
    map["adress"]=adress;
  
    return map;
  }
}
Future<Inscri> createPost(String url, {Map body}) async {
  print("****************************BODY*****************************");
  print(body['mot_de_passe']);
  return http.post(url, body: json.decode(json.encode(body))).then((http.Response response) {
    final int statusCode = response.statusCode;
    print("****************************RESPONSE*****************************");
    print(response.body); 
    if (statusCode < 200 || statusCode > 400 || json == null) {
      throw new Exception("Error while fetching data");
    }
    return Inscri.fromJson(json.decode(response.body));
  });
}

class InscirptionBPage extends StatefulWidget {
  @override
  State createState() => new InscriptionBPageState();
}
class _InscriptionData {
  String name ='';
  String prenom= '';
  String adress='';
  String email = '';
  String password = '';
  String password1 ='';
}
class InscriptionBPageState extends State<InscirptionBPage>
    with SingleTickerProviderStateMixin {
  Animation<double> _iconAnimation;
  AnimationController _iconAnimationController;
  final Future<Inscri> post;
 
  InscriptionBPageState({Key key, this.post}); 
    final _formKey = GlobalKey<FormState>();

  static final url = 'http://192.168.1.2:3000/citoyen/add';
  TextEditingController prenomControler = new TextEditingController();
  TextEditingController nomControler = new TextEditingController();
    TextEditingController emailControler = new TextEditingController();
    TextEditingController mdpControler = new TextEditingController();
    TextEditingController rmdpControler = new TextEditingController();
     TextEditingController adressControler = new TextEditingController();

  _InscriptionData _data = new _InscriptionData();
 String _validateEmail(String value) {
    
    try {
      Validate.isEmail(value);
    } catch (e) {
      return '';
    }

    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 4) {
      return '';
    }

    return null;
  } 
  String _validateName(String value) {
    
    try {
      Validate.isEmail(value);
    } catch (e) {
      return '';
    }

    return null;
  }

  

  @override
  void initState() {
    super.initState();
    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 500));
    _iconAnimation = new CurvedAnimation(
      parent: _iconAnimationController,
      curve: Curves.bounceOut,
    );
    _iconAnimation.addListener(() => this.setState(() {}));
    _iconAnimationController.forward();
  }

  @override
  Widget build(BuildContext context) {
    
    return new Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      body: new Stack(fit: StackFit.expand, children: <Widget>[
        new Image(
          image: new AssetImage("assets/logo1.png"),
          fit: BoxFit.cover,
          colorBlendMode: BlendMode.darken,
          color: Colors.black87,
        ),
        new Theme(
          data: new ThemeData(
              brightness: Brightness.dark,
              inputDecorationTheme: new InputDecorationTheme(
                // hintStyle: new TextStyle(color: Colors.blue, fontSize: 20.0),
                labelStyle:
                    new TextStyle(color: Colors.tealAccent, fontSize: 25.0),
              )),
          isMaterialAppTheme: true,
          child: new ListView(
            //mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              
              new Container(
                padding: const EdgeInsets.all(40.0),
                child: new Form(
                       key: _formKey,

                  autovalidate: true,
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      new TextFormField(
                      controller: prenomControler,

                        decoration: new InputDecoration(

                            labelText: "Nom",
                            hintText: 'Benevole',
                             fillColor: Colors.white),
                        keyboardType: TextInputType.text,
                        validator: this._validateName,
                // onSaved: (String value) {
                //   this._data.email = value;
                // }
                      ),new TextFormField(
                        controller: nomControler,
                        decoration: new InputDecoration(

                            labelText: "Prenom",
                            hintText: 'Benevole',
                             fillColor: Colors.white),
                        keyboardType: TextInputType.text,
                          validator: this._validateName,
                onSaved: (String value) {
                  this._data.email = value;
                }
                      ),
                      new TextFormField(
                        controller: emailControler,
                        decoration: new InputDecoration(

                            labelText: "Email",
                            hintText: 'you@example.com',
                             fillColor: Colors.white),
                        keyboardType: TextInputType.emailAddress,
                         validator: this._validateEmail,
                onSaved: (String value) {
                  this._data.email = value;
                }
                      ),
                       new TextFormField(
                      controller: adressControler,

                        decoration: new InputDecoration(

                            labelText: "Adress",
                            hintText: 'Adress',
                             fillColor: Colors.white),
                        keyboardType: TextInputType.text,
                        validator: this._validateName,
                // onSaved: (String value) {
                //   this._data.email = value;
                // }
                      ),
                      new TextFormField(
                        controller: mdpControler,
                        decoration: new InputDecoration(
                          labelText: "Mot de Passe",
                          hintText: 'Mot de passe',
                          
                        ),
                        obscureText: true,
                        keyboardType: TextInputType.text,
                         validator: this._validatePassword,
                onSaved: (String value) {
                  this._data.password = value;
                }
                      ),
                      Container(
                        child: new TextFormField(
                          controller: rmdpControler,
                         
                          decoration: new InputDecoration(
                            labelText: ("Confirmer le Mot de Passe" ),
                            hintText: 'Mot de passe', 
                             
                            
                          ),
                          
                          obscureText: true,
                          keyboardType: TextInputType.text,
                           validator: this._validatePassword,
                onSaved: (String value) {
                  this._data.password = value;
                }
                        ),
                      ),
                 
                      new Padding(
                        padding: const EdgeInsets.only(top: 60.0),
                      ),
                      new MaterialButton(
                        height: 50.0,
                        minWidth: 150.0,
                        color: Colors.green,
                        splashColor: Colors.teal,
                        textColor: Colors.white,
                        child: new Icon(FontAwesomeIcons.signInAlt),
                        onPressed: () async{
                                
                                Inscri newPost = new Inscri(
                        nom: nomControler.text,
                        prenom: prenomControler.text,
                        email: emailControler.text,
                        adress: adressControler.text,
                        mot_de_passe: mdpControler.text);
                    Inscri p = await createPost(url,
                        body: newPost.toMap());
                    
                                 if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                } 
Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => Modal(),),);
  },), ],
                  ),
                ),
              )
            ], 
          ),
        ),
      ]),
      
    );
  }
  
}