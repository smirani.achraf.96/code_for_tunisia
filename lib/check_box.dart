import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'Login.dart';


class ChekBox extends StatefulWidget {
  @override
  _ChekBoxState createState() => _ChekBoxState();
}

class _ChekBoxState extends State<ChekBox> {
  bool firstVal = false;
  bool secondVal = false;
  bool thirdVal = false;
  bool fourVal = false; 
  bool fiveVal = false; 
  bool sixVal = false; 
  bool sevenVal = false; 
  bool eightVal = false; 
  bool nineVal = false; 
  bool tenVal = false; 
  @override
  Widget build(BuildContext context) {
    return 
       
           Container(
        child: new ListView(
                  scrollDirection: Axis.horizontal,
                  children:<Widget>[ 
                    Container(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" FLUTTER ",textAlign: TextAlign.justify,),
                Checkbox(
                  value: firstVal,
                  onChanged: (bool value) {
                    setState(() {
                      firstVal = value;
                    });
                  },
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" ANDROID "),
                Checkbox(
                  value: secondVal,
                  onChanged: (bool value) {
                    setState(() {
                      secondVal = value;
                    });
                  },
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" JAVA "),
                Checkbox(
                  value: thirdVal,
                  onChanged: (bool value) {
                    setState(() {
                      thirdVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" MACHINE LEARNING "),
                Checkbox(
                  value: fourVal,
                  onChanged: (bool value) {
                    setState(() {
                      fourVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" BLOCKCHAIN "),
                Checkbox(
                  value: fiveVal,
                  onChanged: (bool value) {
                    setState(() {
                      fiveVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" JAVASCRIPT "),
                Checkbox(
                  value: sixVal,
                  onChanged: (bool value) {
                    setState(() {
                      sixVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" PHP "),
                Checkbox(
                  value: sevenVal,
                  onChanged: (bool value) {
                    setState(() {
                      sevenVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" BIG DATA "),
                Checkbox(
                  value: eightVal,
                  onChanged: (bool value) {
                    setState(() {
                      eightVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" NODE JS "),
                Checkbox(
                  value: nineVal,
                  onChanged: (bool value) {
                    setState(() {
                      nineVal = value;
                    });
                  },
                ),
              ],
            ),
             Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(" ANGULAR "),
                Checkbox(
                  value: tenVal,
                  onChanged: (bool value) {
                    setState(() {
                      tenVal = value;
                    });
                  },
                ),
              ],
            ),
          ],
        ),
                    ),
                   
                    
                  ]
        ),
      );
       }
}
