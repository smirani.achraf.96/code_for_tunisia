import 'package:flutter/material.dart';
import 'package:splashscreen/splashscreen.dart';
import 'Slider.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

/* Future<Post> fetchPost() async {
  final response =
      await http.get('http://localhost:3000/evenement/all');

  if (response.statusCode == 200) {
    // If the call to the server was successful, parse the JSON
    return Post.fromJson(json.decode(response.body));
  } else {
    // If that call was not successful, throw an error.
    throw Exception('Failed to load post');
  }
}

class Post {
  final int userId;
  final int id;
  final String title;
  final String body;

  Post({this.userId, this.id, this.title, this.body});

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
      body: json['body'],
    );
  }
}*/

void main(){
  runApp(new MaterialApp(
    home: new MyApp(),
  ));
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
      seconds: 6,
      navigateAfterSeconds: new AfterSplash(),
      
      image: new Image.asset('assets/logobalnc-2.png'),
      backgroundColor: Colors.blueAccent,
      styleTextUnderTheLoader: new TextStyle(),
      photoSize: 100.0,
      onClick: ()=>print("Achraf"),
      loaderColor: Colors.red ,
      
      loadingText: Text("Bienvenue dans  Code For Tunsia",
       style: new TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20.0,
        ),
        ),
        
        
    );
    
  }
}

class AfterSplash extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MySplashScreen();
  }
}
