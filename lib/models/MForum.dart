class MForum {
String title ;  String author; String id ;
   int points; int comments ; bool self ;

  MForum(this.id,this.title ,this.author,this.points,this.comments,this.self);

  MForum.fromJson(Map json)
      : title  = json['title '],
        author = json['author'],
        comments =json['comments '],
       id=json['id'],
       self=json['self'],
        points = json['points'];

  Map toJson() {
    return {'title ': title , 'author': author, 'points': points,'comments': comments,'self': self,'id': id};
  }
}