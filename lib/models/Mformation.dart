class Mformation {
String description;  String titre;
  String lieu;

  Mformation(this.description,this.titre,this.lieu);

  Mformation.fromJson(Map json)
      : description = json['description'],
        titre = json['titre'],
        lieu = json['lieu'];

  Map toJson() {
    return {'description': description, 'titre': titre, 'lieu': lieu};
  }
}