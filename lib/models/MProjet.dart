class MProjet {
String description;  String titre;
  String lien;

  MProjet(this.description,this.titre,this.lien);

  MProjet.fromJson(Map json)
      : description = json['description'],
        titre = json['titre'],
        lien = json['lien'];

  Map toJson() {
    return {'description': description, 'titre': titre, 'lien': lien};
  }
}