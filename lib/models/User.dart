import 'dart:io';

class User {
String detail;  String titre;
  String lieu ; String images ;String id;

  User({this.detail,this.titre,this.lieu,this.images,this.id});
  factory User.fromJson(Map<String, dynamic> json) { 
    return new User (
        detail : json['detail'],
        titre :json['titre'],
        lieu : json['lieu'],
      
        images: json['images'],
        id:json['_id']);
  }
        
  Map toJson() {
    return {'detail': detail, 'titre': titre, 'lieu': lieu,'images':images,'_id':id};
  }
}