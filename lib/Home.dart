import 'package:flutter/material.dart';
import 'events.dart';
import 'projets.dart';
import 'formation.dart';
import 'About.dart';
import 'Contact.dart';
import 'LoginB.dart';
import 'package:code_for_tunisia/intro_page_view.dart';
import 'forum.dart';
import 'dart:ui' as ui;

class HomePage extends StatefulWidget {
  @override
  
  _HomePageState createState() 
  
  {
    return _HomePageState();}
}


class _HomePageState extends State<HomePage>{
  String type;
  String name;
  String email;
  String ff="Achraf Smirani";
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    type=loginState.typeUser;
    name=loginState.nameUser;
    email=loginState.emailUser;
    
     }
     
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Acceuil"), backgroundColor: Colors.redAccent, actions: <Widget>[

       FloatingActionButton(
         heroTag: Null,
              child: Icon(Icons.highlight_off),
              backgroundColor: Colors.redAccent,
              onPressed: (){
                 Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => LoginBPage()));
              }
            ),
            ],
            ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: new Text("Cher Benevole"),
              accountName: new Text('Bienvenue'),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: new AssetImage("assets/logo1.png"),
                ),
                onTap: () => print("This is your current account."),
              ),
              
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new AssetImage("assets/logo1.png"),
                  fit: BoxFit.fill
                )
              ),
            ),
            new ListTile(
              title: new Text("Acceuil"),
              trailing: new Icon(Icons.home),
              onTap: () {
                Navigator.of(context).pop();
                //Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => null));
              }
            ),
            new ListTile(
              title: new Text("A propos de nous"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => About()));
              }
            ),
            new ListTile(
              title: new Text("Les evenements"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Events()));
              }
            ),
            new ListTile(
              title: new Text("Les formations"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Formation()));
              }
            ),
            new ListTile(
              title: new Text("Les projets"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Projets()));
              }
            ),
            new ListTile(
              title: new Text("Blog"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Forum()));
              }
            ),
            new ListTile(
              title: new Text("Contact"),
              
              onTap: () {
                Navigator.of(context).pop();
                Navigator.of(context).push(new MaterialPageRoute(builder: (BuildContext context) => Contact()));
              }
            ),
            
          ],
        ),
      ),
      body: IntroPageView()       
    );
    }
}

