import 'Login.dart';
import 'package:intro_slider/intro_slider.dart';
import 'package:flutter/material.dart';
import 'Home.dart';
 class MySplashScreen extends StatefulWidget {
  MySplashScreen({Key key}) : super(key: key);

  @override
  MySplashScreenState createState() => new MySplashScreenState();
}

class MySplashScreenState extends State<MySplashScreen> {
  List<Slide> slides = new List();

  @override
  void initState() {
    super.initState();

    slides.add(
      new Slide(
        title: "Bienvenue",
        description: "Code For Tunisia est une organisation non gouvernementale tunisienne qui a pour but d'aider la societé à apprendre le developpement & à presenter des projets qui  aident le gouvernement à travailler pour les personnes qui en ont le plus besoin.",
        pathImage: "assets/logobalnc-2.png",
        backgroundColor: Color(0xfff5a623),
      ),
    );
    slides.add(
      new Slide(
        title: "Les Formations",
        description: "L'association Code For Tunisia presente plusieurs formations aux citoyens inscrits assuré par des formatteurs competents ",
        pathImage: "assets/formation.png",
        backgroundColor: Color(0xffDD8471),
      ),
    );
    slides.add(
      new Slide(
        title: "Les Projets",
        description:
            "La technologie et le gouvernement sont les deux principaux leviers d'amélioration de la vie des personnes. Nous les mettons ensemble en realisant des projets où le gouvernement travaille pour le peuple, par le peuple, à l'ère numérique",
        pathImage: "assets/projet.png",
        backgroundColor: Color(0xff9932CC),
      ),
    );
    slides.add(
      new Slide(
        title: "Les Evenements",
        description: "L'association Code For Tunisia orhanise de façon reguliere des evenements de divers type qui offrent aux membres l'occasion de s'integrer dans la vie associative ",
        pathImage: "assets/evenement.jpg",
        backgroundColor: Color(0xff208542),
      ),
    );
  }

  void onDonePress() {
    Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => LoginPage(),),);
 }
  void onSkipPress()
  {
Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => LoginPage(),),);
  }

  @override
  Widget build(BuildContext context) {
    return new IntroSlider(
      slides: this.slides,
      onDonePress: this.onDonePress,
      onSkipPress: this.onSkipPress,
    );
  }
}